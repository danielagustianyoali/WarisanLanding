<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

use Session, Socialite;

class LandingController extends Controller
{
    //
    public function homepage(Request $request){

        if ($request->has('utm_medium')) {
            $request->session()->put('utm_medium', $request->utm_medium);
        }
        if ($request->has('utm_campaign')) {
            $request->session()->put('utm_campaign', $request->utm_campaign);
        }
        if ($request->has('utm_source')) {
            $request->session()->put('utm_source', $request->utm_source);
        }
        if ($request->has('utm_content')) {
            $request->session()->put('utm_content', $request->utm_content);
        }

        $data = null;
        if ($request->has('ref')) {
            $data = $this->getReferalUser($request->ref);

            if ($data != null && $request->ref != "") {
                $request->session()->put('referral_code', $request->ref);
            } else {
                $data = null;
                // to delete query params [ref]
                return redirect()->route('homepage')->with("errorMsg", "Kode Referral tidak ditemukan!");
            }
        }else{
            if(Session::has('referral_code')){
                Session::forget('referral_code');
            }
        }
        return view('pages.homepage', compact('data'));
    }
    public function postRegister(Request $request){
        $client = new Client();
        $formBody = [
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->confirm_password,
            'name' => $request->name,
            'whatsapp' => '62' . $request->whatsapp
        ];

        if (Session::get('referral_code')) {
            $formBody['referral'] = Session::get('referral_code');
        }
        if (Session::has('utm_medium')) {
            $formBody['utm_medium'] = Session::get('utm_medium');
        }
        if (Session::has('utm_campaign')) {
            $formBody['utm_campaign'] = Session::get('utm_campaign');
        }
        if (Session::has('utm_source')) {
            $formBody['utm_source'] = Session::get('utm_source');
        }
        if (Session::has('utm_content')) {
            $formBody['utm_content'] = Session::get('utm_content');
        }

        // dd($formBody);

        try {
            $client = $client->post( 'https://api.warisan.co.id/api/v1/register', [
                'headers' => [
                    'X-CSRF-Token' => $request->_token,
                ],
                'form_params' => $formBody
            ]);

            if ($client->getStatusCode() == '200') {

                $res = json_decode($client->getBody());
                //dd($res);
                $data = $res->data;
                $request->session()->put('token', $res->access_token);
                $request->session()->put('userID', $data->id);
                $request->session()->put('name', $data->name);
                $request->session()->put('email', $data->email);
                $request->session()->put('username', $data->username);
                $request->session()->put('photo', null);
                $request->session()->put('whatsapp', $res->data->whatsapp);
                $request->session()->put('referral_code', "" );
                // $request->session()->put('is_verify', 0);
                $request->session()->put('ktp_verification', 0);
                $request->session()->put('whatsapp_verification', 0);
                $request->session()->put('email_verification', 0);
                $request->session()->put('package_id', 0);
                $request->session()->put('hasPin', 0);
                $url = "https://warisan.co.id/dashboard";
                return \Redirect::to($url);
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            $res = json_decode($e->getResponse()->getBody()->getContents());
            
            $resp = collect($res);
            //dd($resp);
            if ($resp->has('status')) {
                $message = $res->message;
                //return redirect()->back();
                if (isset($message->email) && isset($message->whatsapp)) {
                    return redirect()->back()->with('errorAll', "Email dan Nomor Whatsapp anda salah")->withInput();
                } else if (isset($message->email) && !isset($message->whatsapp)) {
                    return redirect()->back()->with('errorMessage', "Email telah digunakan")->withInput();
                } else if (!isset($message->email) && isset($message->whatsapp)) {
                    return redirect()->back()->with('errorMessage', "Nomor whatsapp minimal 6 karakter")->withInput();
                }
            }
        }
    }
    public function getReferalUser($ref)
    {
        $client = new Client();
        try {
            $client = $client->get('https://api.warisan.co.id/api/v1/referral-user?referral=' . $ref);
            if ($client->getStatusCode() == '200') {
                $res = json_decode($client->getBody());
                // dd($res);
                // return $res->data->name;

                return $res->data;
            }
        } catch (BadResponseException $e) {
            $exception = handleError($e);
            return "Referal error";
        }
    }
}
