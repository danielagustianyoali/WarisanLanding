
<div class="bergabung-div">
    <div class="card">
        <div class="bulet__content">
            <img src="{{$url}}" alt="">
        </div>
        <h6>{{$title}}</h6>
        <p>{{$detail}}</p>
    </div>
</div>
