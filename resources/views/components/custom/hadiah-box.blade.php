<div class="hadiah-box">
    <div class="card">
        <div class="circle-img">
            <img src="{{ $url }}" alt="">
        </div>
        <div class="card-body">
            <div class="d-flex flex-column justify-content-between h-100">
                <div>
                    <h3 class="title">{{ $title }}</h3>
                    <h5 class="mt-4 mb-2">Syarat</h5>
                    {!! $detail !!}
                </div>
                <div class="mt-3 d-flex justify-content-center">
                    <button class="btn btn__prim btn-bergabung">
                        Bergabung
                    </button>
                </div>
            </div>


        </div>
    </div>
</div>
