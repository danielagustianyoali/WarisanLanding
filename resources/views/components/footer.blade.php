<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">

    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-around mb-5">

        <!--begin::Copyright-->
        <div class="text-dark order-1 order-md-1">
            <img src="{{ asset('images/nav-logo-white.svg') }}" alt="" class="img-footer mt-4">
            <h3 class="mt-5 ikuti-kami">Ikuti Sosial Media Kami:</h3>
            <div class="d-flex justify-content-center mt-4">
                <a href="">
                    <img src="https://seeklogo.com/images/F/facebook-logo-966BBFBC34-seeklogo.com.png" alt=""
                        class="img-icon">
                </a>
                <a href="">
                    <img src="https://seeklogo.com/images/Y/youtube-icon-logo-521820CDD7-seeklogo.com.png" alt=""
                        class="img-icon">
                </a>
                <a href="">
                    <img src="https://seeklogo.com/images/I/instagram-new-2016-logo-D9D42A0AD4-seeklogo.com.png" alt=""
                        class="img-icon">
                </a>
            </div>
        </div>

        <!--end::Copyright-->

        <!--begin::Nav-->
        <div class=" order-2 order-md-2 reseller-footer">
            <div class="mt-5">
                <h4 class="text-white">Ingin Menjadi Reseller?</h4>
                <div class="display-mobile">
                    <a href="" class="btn btn__white text-center">Gabung Sekarang</a>
                </div>
            </div>
            <div class="mt-5 mb-5">
                <h4 class="text-white">Tentang Kami</h4>
                <div class="display-mobile">
                    <a href="" class="btn btn__white text-center">Profil Warisan</a>

                </div>
            </div>

        </div>

        <!--end::Nav-->
    </div>

    <!--end::Container-->
</div>

<!--end::Footer-->
