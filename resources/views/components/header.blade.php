<!--begin::Header-->
<div id="kt_header" class="header header-fixed">

    <!--begin::Container-->
    <div class="container d-flex align-items-stretch justify-content-between">

        <!--begin::Left-->
        <div class="d-flex align-items-stretch mr-3 justify-content-between w-100">

            <!--begin::Header Logo-->
            <div class="header-logo">
                <a href="index.html">
                    <img alt="Logo" src="{{asset('images/nav-logo.svg')}}" class="logo-default max-h-40px" />
                    <img alt="Logo" src="{{asset('images/nav-logo.svg')}}" class="logo-sticky max-h-40px" />
                </a>
            </div>

            <!--end::Header Logo-->

            <!--begin::Header Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">

                <!--begin::Header Menu-->
                <div id="kt_header_menu"
                    class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">

                    <!--begin::Header Nav-->
                    {{-- <ul class="menu-nav">
                        <li class="menu-item  menu-item-here ">
                            <a href="javascript:;" class="menu-link ">
                                <span class="menu-text">Masuk</span>
                                <i class="menu-arrow"></i>
                            </a>

                        </li>
                        <li class="menu-item  ">
                            <a href="https://google.com" class="menu-link ">
                                <span class="menu-text">Bergabung</span>
                                <span class="menu-desc"></span>
                                <i class="menu-arrow"></i>
                            </a>

                        </li>
                        
                    </ul> --}}

                    <!--end::Header Nav-->
                </div>

                <!--end::Header Menu-->
            </div>

            <!--end::Header Menu Wrapper-->
        </div>

        <!--end::Left-->

        <!--begin::Topbar-->
        

        <!--end::Topbar-->
    </div>

    <!--end::Container-->
</div>

<!--end::Header-->
