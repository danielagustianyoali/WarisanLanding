<!--begin::Entry-->
@extends('master')

@section('cssinline')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
@endsection


@section('content')
    <div class="fixed-whatsapp">
        <div class="d-flex align-items-center">
            {{-- <div class="whatsapp-text mr-3">Kami siap membantu anda <br> menjadi perempuan panutan</div> --}}
            <a class="" href="#">
                <div class="box-whatsapp">
                    <i class="fab fa-whatsapp" style="font-size: 50px; color: white;"></i>
        
                </div>
            </a>
        </div>
        
    </div>
    
    <section class="banner-hero position-relative">
        <div class="background-desktop position-absolute">
            <div class="red">

            </div>
            <div class="white"></div>
        </div>
        <div class="container">
            <div class="row no-gutters w-100">
                <div class="col-lg-7 col-md-6 banner-info my-auto">
                    <div class="container ">
                        <h1 class="title">Perempuan Panutan Dimulai
                            dari <span>KAMU</span>
                        </h1>
                        <p class="desc mt-8">
                            Menjadi sosok hebat bisa dimulai dari hal-hal kecil. WARISAN hadir
                            menjadi wadah <strong>Reseller</strong> untuk seluruh Perempuan yang siap menjadi
                            Panutan. Kamukah orangnya?
                        </p>
                        <div class="d-flex mt-5">
                            <button class="btn btn__white mr-3 btn-bergabung">
                                Bergabung
                            </button>
                            <a class="btn btn__bordered_prim" href="https://warisan.co.id/warisan-kami">
                                Produk Reseller
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="half-circle ">
                        <div class="d-flex align-items-center justify-content-center " style="height: 100% ">
                            <div class="swiper mySwiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img src="{{ asset('images/emas.png') }}" alt="" class="img-slide">
                                    </div>
                                    <div class="swiper-slide"><img src="{{ asset('images/mobil.png') }}" alt=""
                                            class="img-slide"></div>
                                    <div class="swiper-slide"><img src="{{ asset('images/scoopy.png') }}" alt=""
                                            class="img-slide"></div>

                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        </div>

                        {{-- <div class="d-flex align-items-center justify-content-center" style="height: 20% ">
                                <a href="" class="btn btn__prim">Read More</a>
                            </div> --}}




                    </div>
                </div>
            </div>
        </div>


    </section>
    <!--begin::Container-->
    <div class="divider"></div>
    <section class="section-1">

        <div class="container">
            <h2 class="title-part text-center">Mengapa Kamu Harus Bergabung di <span>Keluarga Warisan</span> ?</h2>

            <p class="detail-part text-center">
                Karena cuma disini kamu bisa mendapatkan keuntungan <br>
                melimpah dengan modal sedikit. Gak percaya? </p>


            <div class="row ">
                <div class="col-lg-5  my-auto">
                    <div class="position-relative">
                        <img src="{{ asset('images/laptop.png') }}" alt="" class="img-section-1">
                        <iframe class="movie-frame" src="https://www.youtube.com/embed/0t0lnIB18x0?enablejsapi=1"
                            frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-lg-7 ">
                    <div class="row">
                        <div class="col-md-3 col-6 h-100">

                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/tidak_perlu.svg
                                @endslot
                                @slot('title')
                                    Tidak Perlu Stok
                                @endslot
                                @slot('detail')
                                    Gak perlu ribet stok barang di rumah
                                @endslot
                            @endcomponent
                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/komisi_jual.svg
                                @endslot
                                @slot('title')
                                    Komisi Penjualan Besar
                                @endslot
                                @slot('detail')
                                    Mendapatkan keuntungan maksimal dari hasil penjualan
                                @endslot
                            @endcomponent
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="margin-bergabung"></div>
                            @component('components.custom.bergabung-box')
                                @slot('url')
                                https://warisan.co.id/images/ornament/produk_mudah.svg @endslot
                                @slot('title')
                                    Produk Mudah Dijual
                                @endslot
                                @slot('detail')
                                    Dijamin semua barang mudah dijual
                                @endslot
                            @endcomponent
                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/media_promosi.svg
                                @endslot
                                @slot('title')
                                    Disiapkan Media Promosi
                                @endslot
                                @slot('detail')
                                Gak perlu pusing mikirin iklan dan materi promosi, semuanya sudah tersedia @endslot
                            @endcomponent
                        </div>
                        <div class="col-md-3 col-6">

                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/teknologi_canggih.svg
                                @endslot
                                @slot('title')
                                    Teknologi Canggih
                                @endslot
                                @slot('detail')
                                    Sistem saling terintegrasi sehinga mempemudah proses jualan
                                @endslot
                            @endcomponent
                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/kom_positif.svg
                                @endslot
                                @slot('title')
                                    Membentuk Komunitas Positif
                                @endslot
                                @slot('detail')
                                    Tempat belajar dan bertumbuh bersama, membawa dampak positif
                                @endslot
                            @endcomponent
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="margin-bergabung"></div>
                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/bisa_dilakukan.svg
                                @endslot
                                @slot('title')
                                    Bisa Dilakukan di Rumah
                                @endslot
                                @slot('detail')
                                    Gak perlu ribet pergi ke kantor untuk menghasilkan cuan
                                @endslot
                            @endcomponent
                            @component('components.custom.bergabung-box')
                                @slot('url')
                                    https://warisan.co.id/images/ornament/dapat_edukasi.svg
                                @endslot
                                @slot('title')
                                    Mendapatkan Edukasi
                                @endslot
                                @slot('detail')
                                    Jangan takut gagal, karena akan dilatih oleh orang-orang profesional
                                @endslot
                            @endcomponent
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </section>
    <div class="divider"></div>
    <section class="section-2">
        <div class="d-flex justify-content-center w-100">
            <div class="container pt-8 pb-7">
                <h2 class="title-white text-center ">Tingkatkan Level Membermu dengan cara menjadi <br> reseller aktif dan
                    menangkan
                    hadiah berikut
                    <br>
                    <span>tanpa diundi!!!</span>
                </h2>
                <div class="row mt-10 mb-5">
                    <div class="col-md-4 mb-4">
                        @component('components.custom.hadiah-box')

                            @slot('url')
                                {{ asset('images/mobil.png') }}
                            @endslot
                            @slot('title')
                                Lv. Srikandi
                            @endslot
                            @slot('detail')
                                <p class="syarat">Memiliki 5 Tim peraih reward <strong>Wedari</strong></p>
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-md-4 mb-4">
                        @component('components.custom.hadiah-box')

                            @slot('url')
                                {{ asset('images/scoopy.png') }}
                            @endslot
                            @slot('title')
                                Lv. Wedari
                            @endslot
                            @slot('detail')
                                <p class="syarat">Memiliki 5 Tim peraih reward <strong>Kartini</strong></p>
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-md-4 mb-4">
                        @component('components.custom.hadiah-box')

                            @slot('url')
                                {{ asset('images/emas.png') }}
                            @endslot
                            @slot('title')
                                Lv. Kartini
                            @endslot
                            @slot('detail')
                                <p>Penjualan pribadi
                                    skincare gwiyomi
                                    20jt & Tim reseller
                                    skincare gwiyomi
                                    60jt</p>
                            @endslot
                        @endcomponent
                    </div>

                </div>
            </div>
        </div>

    </section>
    <div class="divider"></div>
    <section class="section-3">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-6">
                    <img src="{{asset('images/warisan-example.png')}}" alt="" class="img-section-3">

                </div>
                <div class="col-md-6 my-auto">
                    <h1 class="title-part mb-4"> Gabung Keluarga Warisan
                        PANEN UNTUNG!</h1>
                    <div class="d-flex mb-3">
                        <div class="backg-primary mr-5">
                            <img src="https://warisan.co.id/images/ornament/lp_mudah.svg" alt="" class="div-ornament">
                        </div>
                        <div>
                            <h5 class="title-part mb-1">Mudah</h5>
                            <p class="detail-part mb-0">Semua sudah tersedia dalam satu platform. Mulai dari stok barang,
                                sistem pembayaran, hingga media promosi.
                            </p>
                        </div>
                    </div>
                    <div class="d-flex mb-3">
                        <div class="backg-primary  mr-5">
                            <img src="https://warisan.co.id/images/ornament/lp_hemat2.svg" alt="" class="div-ornament">
                        </div>
                        <div>
                            <h5 class="title-part mb-1">HEMAT
                            </h5>
                            <p class="detail-part mb-0">Hanya dengan Rp 299.000 langsung mendapatkan bimbingan dari Mentor
                                Profesional.
                            </p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-3">
                        <div class="backg-primary  mr-5">
                            <img src="https://warisan.co.id/images/ornament/lp_untung.svg" alt="" class="div-ornament">
                        </div>
                        <div>
                            <h5 class="title-part mb-1">UNTUNG
                            </h5>
                            <p class="detail-part mb-0">Modal mulai dari Rp 0 dijamin dapat untung berkali-kali lipat.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-4">
                <button class="btn btn__prim btn-bergabung">
                    Bergabung
                </button>
            </div>
        </div>
    </section>
    <div class="divider"></div>
    <section class="section-bergabung ">
        <div class="container">
            <h1 class="title-part text-center mb-6"> Yuk Ikutan Bareng Kami!</h1>
            <div class="w-100 d-flex justify-content-center">
                <div class="card ">
                    <div class="card-body">
                        <form action="{{ route('postRegister') }}" class="register-form" method="POST">
                            @csrf
                            <img src="{{ asset('images/nav-logo.svg') }}" alt="" class="nav-logo-form">
                            <button class="btn button-google w-100 mt-5 mb-7" type="button">
                                <img src="https://warisan.co.id/images/web-icons/google.svg" alt="" class="mr-4">
                                Sign in With Google
                            </button>
                            <div class="w-100 my-4">
                                <div class="divider-reg mt-5"></div>
                                <p class="text-atau">Atau</p>
                            </div>
                            <div class="input-icon">
                                <input type="text" class="form-control" name="name" placeholder="Nama" required>
                                <span>
                                    <img src="https://warisan.co.id/images/web-icons/user.svg" alt="">
                                </span>
                            </div>
                            <div class="fonts-12 fc-danger error gutter-b" id="error_name"></div>

                            <div class="input-icon">
                                <input type="text" class="form-control" name="email" placeholder="Email">
                                <span>
                                    <img src="https://warisan.co.id/images/web-icons/mail.svg" alt="" required>
                                </span>
                            </div>
                            <div class="fonts-12 fc-danger error gutter-b" id="error_email"></div>

                            <div class="input-icon">
                                <input type="text" class="form-control" name="whatsapp" placeholder="No. Whatsapp"
                                    required>
                                <span>
                                    +62
                                </span>
                            </div>
                            <div class="fonts-12 fc-danger error gutter-b" id="error_phone"></div>

                            <div class="input-icon">
                                <input type="password" class="form-control" name="password" placeholder="Password"
                                    required>
                                <span>
                                    <img src="https://warisan.co.id/images/web-icons/key.svg" alt="">
                                </span>
                            </div>
                            <div class="fonts-12 fc-danger error gutter-b" id="error_password"></div>

                            <div class="input-icon">
                                <input type="password" class="form-control" name="confirm_password"
                                    placeholder="Confirm Password" required>
                                <span>
                                    <img src="https://warisan.co.id/images/web-icons/key.svg" alt="">
                                </span>
                            </div>
                            <div class="fonts-12 fc-danger error gutter-b" id="error_confirm_password"></div>

                            <p style="color: #00000030;" class="mt-5">Perempuan Panutan</p>
                            <div class="input-icon">
                                @if (Session::has('referral_code'))
                                    <input type="text" class="form-control" name="referral" placeholder="Referral ID"
                                        readonly value="{{ Session::get('referral_code') }}">
                                @else
                                    <input type="text" class="form-control" name="referral" placeholder="Referral ID"
                                        readonly >
                                @endif

                                <span>
                                    <img src="https://warisan.co.id/images/web-icons/input_referal.svg" alt="">
                                </span>
                                <div class="edit-referral position-absolute" onclick="editReferral()">Edit</div>
                            </div>

                            <button class="my-6 btn btn__prim w-100">
                                Daftar
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <!--end::Container-->

@endsection


@section('jsPage')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="{{ asset('asset/plugins/jquery-mask/jquery.mask.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.all.min.js"></script>
    @if (Session::has('errorMsg'))
        <script>
            Swal.fire('Gagal Mengirim Data', '{{ Session::get('errorMsg') }}', 'error');
        </script>
    @endif
    @if (Session::has('errorAll'))
        <script>
            Swal.fire('Gagal Mengirim Data', 'Email dan Whatsapp Kamu Telah Dipakai!', 'error');
        </script>
    @endif
    @if (Session::has('errorMessage'))
        <script>
            Swal.fire('Gagal Mengirim Data', '{{ Session::get('errorMessage') }}', 'error');
        </script>
    @endif
    <script>
        var swiper = new Swiper(".mySwiper", {
            autoplay: {
                delay: 2500,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });

        function validateNumber(number) {
            var reg = /^\d+$/;
            return reg.test(number);
        }

        function validateEmail(email) {
            const re =
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
    </script>
    <script>
        $(function() {

            $('.btn-bergabung').click(function() {
                $('html, body').animate({
                    scrollTop: $(".section-bergabung").offset().top
                }, 1500);
            });
        })
    </script>
    <script>
        $(document).ready(function() {
            $('input[name="whatsapp"]').mask('000-0000-00000');
        });
    </script>
    <script>
        // Validation
        $(function() {
            $('.register-form').on('submit', function(e) {
                //e.preventDefault();
                let errorEmail = false
                let errorPassword = false
                let errorConfirmPassword = false
                let errorName = false
                let errorPhone = false
                $('#error_email').empty()
                $('#error_password').empty()
                $('#error_confirm_password').empty()
                $('#error_phone').empty()
                $('#error_name').empty()
                const email = $('input[name="email"]').val()
                const password = $('input[name="password"]').val()
                const confirm_password = $('input[name="confirm_password"]').val()
                const phone = $('input[name="whatsapp"]').unmask().val()

                const name = $('input[name="name"]').val()

                $('input[name="whatsapp"]').mask('000-0000-00000');

                // console.log(phone);
                if (email.length === 0) {
                    errorEmail = true
                    $('#error_email').text('Field tidak boleh kosong!')
                } else {
                    if (!validateEmail(email)) {
                        errorEmail = true
                        $('#error_email').text('Email tidak valid!')
                    }
                }

                if (password.length === 0) {
                    errorPassword = true
                    $('#error_password').text('Field tidak boleh kosong!')
                } else {
                    if (password.length < 8) {
                        errorPassword = true
                        $('#error_password').text('Minimal 8 character!')
                    } else if (confirm_password !== password) {
                        errorPassword = true
                        $('#error_password').text('Password tidak sama!')
                    }
                }

                if (confirm_password.length === 0) {
                    errorConfirmPassword = true
                    $('#error_confirm_password').text('Field tidak boleh kosong!')
                } else {
                    if (confirm_password.length < 8) {
                        errorConfirmPassword = true
                        $('#error_confirm_password').text('Minimal 8 character!')
                    } else if (confirm_password !== password) {
                        errorConfirmPassword = true
                        $('#error_confirm_password').text('Password tidak sama!')
                    }
                }

                if (name.length === 0) {
                    errorName = true
                    $('#error_name').text('Field tidak boleh kosong!')
                }

                if (phone.length < 8) {
                    errorPhone = true
                    $('#error_phone').text('Nomor Handphone tidak valid!')
                } else {
                    if (!validateNumber(phone)) {
                        errorPhone = true
                        $('#error_phone').text('Nomor tidak valid!')
                    }
                }

                if (errorPassword || errorEmail || errorPhone || errorName || errorConfirmPassword) {
                    e.preventDefault();
                } else {
                    // $('.text__btn').addClass('d-none')
                    // $('.spinner').removeClass('d-none')
                    // $('#tombol_masuk').prop('disabled', true);
                }

            })
        })
    </script>
    <script>
        var edit = true
        var urlParams = new URLSearchParams(window.location.search);
        var ref_code = '';

        if (urlParams.has('ref')) {
            ref_code = urlParams.get('ref')
        }

        function editReferral() {
            edit = !edit

            $('input[name="referral"]').attr('readonly', edit);

            if (!edit) {
                $('input[name="referral"]').val(ref_code)
                $('.edit-referral').text("Done")
            } else {
                let newRef = $('input[name="referral"]').val()
                $('input[name="referral"]').val(newRef)

                window.location.href = `{{ route('homepage') }}?ref=${newRef}`

                $('.edit-referral').text("Edit")
            }
        }
    </script>
@endsection


<!--end::Entry-->
